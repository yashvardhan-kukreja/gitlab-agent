package usage_metrics

import (
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/stretchr/testify/require"
)

var (
	_ Counter               = &counter{}
	_ UsageTrackerInterface = &UsageTracker{}
)

func TestUsageTracker(t *testing.T) {
	u := NewUsageTracker()
	c := u.RegisterCounter("x")
	require.Contains(t, u.counters, "x")
	s := u.RegisterUniqueCounter("y")
	require.Contains(t, u.uniqueCounters, "y")
	z1 := u.RegisterIntGroupedUniqueCounter("z1")
	require.Contains(t, u.intGroupedUniqueCounters, "z1")
	z2 := u.RegisterIntGroupedUniqueCounter("z2")
	require.Contains(t, u.intGroupedUniqueCounters, "z2")

	ud := u.CloneUsageData()
	expectedCounters := map[string]int64{}
	require.Equal(t, expectedCounters, ud.Counters)
	expectedUniqueCounters := map[string][]int64{}
	require.Equal(t, expectedUniqueCounters, ud.UniqueCounters)
	expectedIntGroupedUniqueCounters := map[string]map[int64][]int64{}
	require.Equal(t, expectedIntGroupedUniqueCounters, ud.IntGroupedUniqueCounters)

	c.Inc()
	s.Add(1)
	s.Add(3)
	z1.Add(123, 1)
	z1.Add(123, 3)
	z1.Add(456, 5)
	z2.Add(789, 1)
	ud = u.CloneUsageData()
	expectedCounters = map[string]int64{
		"x": 1,
	}
	require.Equal(t, expectedCounters, ud.Counters)
	expectedUniqueCounters = map[string][]int64{
		"y": {3, 1},
	}
	requireEqual(t, expectedUniqueCounters, ud)
	expectedIntGroupedUniqueCounters = map[string]map[int64][]int64{
		"z1": {
			123: {1, 3},
			456: {5},
		},
		"z2": {
			789: {1},
		},
	}

	// Sort the slice to avoid a flaky test.
	for _, setGroups := range ud.IntGroupedUniqueCounters {
		for _, s := range setGroups {
			sort.Slice(s, func(i, j int) bool {
				return s[i] < s[j]
			})
		}
	}
	require.Equal(t, expectedIntGroupedUniqueCounters, ud.IntGroupedUniqueCounters)

	u.Subtract(ud)
	ud = u.CloneUsageData()
	require.Empty(t, ud.Counters)
	require.Empty(t, ud.UniqueCounters)
	require.Empty(t, ud.IntGroupedUniqueCounters)
}

func requireEqual(t *testing.T, expectedUniqueCounters map[string][]int64, ud *UsageData) {
	require.Empty(t, cmp.Diff(expectedUniqueCounters, ud.UniqueCounters, cmpopts.SortSlices(func(x, y int64) bool { return x < y })))
}
